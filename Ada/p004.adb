------------------------------------------------------------------------
-- Filename: p004.adb
--
-- Description:
--   Project Euler Problem 4: Largest palindrome product
-- 
-- Author: Alexander Gosselin
-- E-mail: <alexander.gosselin@alumni.ubc.ca>
--         <alex@oddloop.ca>
-- Date:   March 31, 2018
--
-- Link:   <https://projecteuler.net/problem=4>
-- 
-- Problem statement:
--   A palindromic number reads the same both ways. The largest 
--   palindrome made from the product of two 2-digit numbers is 
--   9009 = 91 × 99.
--   Find the largest palindrome made from the product of two 3-digit
--   numbers.
------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Strings.Fixed;

procedure P004 is

   function Is_Palindrome (S : String) return Boolean is
   begin
      for I in 0 .. S'Length / 2 loop
         if S (S'First + I) /= S (S'Last - I) then
            return False;
         end if;
      end loop;
      return True;
   end Is_Palindrome;

   function Is_Palindrome (N : Natural) return Boolean is
     (Is_Palindrome (Ada.Strings.Fixed.Trim 
		       (Source => N'Image,
                        Side   => Ada.Strings.Left)));

   function Largest_Palindrome_Product (Min, Max : in Natural)
				       return Natural is
      -- Return the largest palindromic product of two numbers, each in
      -- the interval from Min to Max.
      A, B : Natural;
      Product : Natural;
   begin
      for I in reverse Min .. Max loop
         A := I;
         B := I;
         while A <= Max and B >= Min loop
            Product := A * B;
            if Is_Palindrome (Product) then
               return Product;
            end if;
            A := A + 1;
            B := B - 1;
         end loop;
         A := I;
         B := I - 1;
         while A <= Max and B >= Min loop
            Product := A * B;
            if Is_Palindrome (Product) then
               return Product;
            end if;
            A := A + 1;
            B := B - 1;
         end loop;
      end loop;
      return 0;
   end Largest_Palindrome_Product;

   D : Natural := 3;
   Min : Natural := 10**(D - 1);
   Max : Natural := 10**D - 1;

   Result : Natural := Largest_Palindrome_Product (Min, Max);

begin

   Ada.Text_IO.Put_Line (Item => Result'Image);

end P004;
