------------------------------------------------------------------------
-- Filename: p005.adb
--
-- Description:
--   Project Euler Problem 5: Smallest multiple
-- 
-- Author: Alexander Gosselin
-- E-mail: <alexander.gosselin@alumni.ubc.ca>
--         <alex@oddloop.ca>
-- Date:   March 31, 2018  
--
-- Link:   <https://projecteuler.net/problem=5>
-- 
-- Problem statement:
--   2520 is the smallest number that can be divided by each of the
--   numbers from 1 to 10 without any remainder.
--   What is the smallest positive number that is evenly divisible by
--   all of the numbers from 1 to 20?
------------------------------------------------------------------------

with Ada.Text_IO;

procedure P005 is

   -- Greatest Common Divisor
   function GCD (A, B : in Natural) return Natural is
      R : Natural := A; -- Remainder
      D : Natural := B; -- Divisor
      T : Natural;      -- Temporary
   begin
      while R > 0 loop
         T := R;
         R := D rem R;
         D := T;
      end loop;
      return D;
   end GCD;

   -- Least Common Multiple
   function LCM (A, B : in Natural) return Natural is
      (A * B / GCD (A, B));

   -- LCM of all numbers between Min and Max.
   function LCM_Range (Min, Max : in Natural) return Natural is
      M : Natural := 1; -- Multiple
   begin
      for I in Natural range Min .. Max loop
         M := LCM (M, I);
      end loop;
      return M;
   end LCM_Range;

   N : Natural := 20;

   Result : Natural := LCM_Range (1, N);

begin

   Ada.Text_IO.Put_Line (Result'Image);

end P005;
