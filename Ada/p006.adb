------------------------------------------------------------------------
-- Filename: p006.adb
--
-- Description:
--   Project Euler Problem 6: Sum square difference
-- 
-- Author: Alexander Gosselin
-- E-mail: <alexander.gosselin@alumni.ubc.ca>
--         <alex@oddloop.ca>
-- Date:   March 31, 2018
--
-- Link:   <https://projecteuler.net/problem=6>
-- 
-- Problem statement:
--   The sum of the squares of the first ten natural numbers is,
--     12 + 22 + ... + 102 = 385
--   The square of the sum of the first ten natural numbers is,
--     (1 + 2 + ... + 10)2 = 552 = 3025
--   Hence the difference between the sum of the squares of the first
--   ten natural numbers and the square of the sum is 3025 − 385 = 2640.
--   Find the difference between the sum of the squares of the first one
--   hundred natural numbers and the square of the sum.
------------------------------------------------------------------------

with Ada.Text_IO;

procedure P006 is

   -- Sum up to N.
   -- function Triangle_Number (N : in Natural) return Natural is
   --   (N * (N + 1) / 2);
   --
   -- Sum of squares up to N^2.
   -- function Pyramid_Number (N : in Natural) return Natural is
   --   (N * (N + 1) * (2*N + 1) / 6);
   --
   -- function Sum_Square_Difference (N : in Natural) return Natural is
   --   (Pyramid_Number (N) - (Triangle_Number (N))**2);

   function Sum_Square_Difference (N : in Natural) return Natural is
     (N * (N + 1) * (3 * N * (N + 1) - (4 * N + 2)) / 12);

   N : Natural := 100;

   Result : Natural := Sum_Square_Difference (N);

begin

   Ada.Text_IO.Put_Line (Result'Image);

end P006;
