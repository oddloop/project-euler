------------------------------------------------------------------------
-- Filename: p003.adb
--
-- Description:
--   Project Euler Problem 3: Largest prime factor
-- 
-- Author: Alexander Gosselin
-- E-mail: <alexander.gosselin@alumni.ubc.ca>
--         <alex@oddloop.ca>
-- Date:   March 31, 2018
--
-- Link:   <https://projecteuler.net/problem=3>
-- 
-- Problem statement:
--   The prime factors of 13195 are 5, 7, 13 and 29.
--   What is the largest prime factor of the number 600851475143?
------------------------------------------------------------------------

with Ada.Text_IO;

procedure P003 is

   type Integer is new Long_Long_Integer;
   subtype Natural is Integer range 0 .. Integer'Last;

   function Largest_Prime_Factor (N : in Natural)
     return Natural is
      P : Natural := N;
      D : Natural := 3;
   begin
      while P rem 2 = 0 and P > 2 loop
         P := P / 2;
      end loop;
      while D < P loop
         if P rem D = 0 then
            P := P / D;
         else
            D := D + 2;
         end if;
      end loop;
      return P;
   end Largest_Prime_Factor;

   N : Natural := 600_851_475_143;

   Result : Natural := Largest_Prime_Factor (N);

begin

   Ada.Text_IO.Put_Line (Result'Image);

end P003;
