------------------------------------------------------------------------
-- Filename: p001.adb
--
-- Description:
--   Project Euler Problem 1: Multiples of 3 and 5
-- 
-- Author: Alexander Gosselin
-- E-mail: <alexander.gosselin@alumni.ubc.ca>
--         <alex@oddloop.ca>
-- Date:   July 20, 2018  
--
-- Link:   <https://projecteuler.net/problem=1>
-- 
-- Problem statement:
--   If we list all the natural numbers below 10 that are multiples of 3
--   or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
--   Find the sum of all the multiples of 3 or 5 below 1000.
------------------------------------------------------------------------

with Ada.Text_IO;

procedure P001 is
   
   -- Greatest Common Divisor
   function GCD (A : Positive; B : Natural) return Positive is
     (if B = 0 then A else GCD (B, A rem B));
   
   -- Least Common Multiple
   function LCM (A, B : Positive) return Positive is
     ((A * B) / GCD (A, B));
    
   function Triangle_Number (N : Natural) return Natural is
     (N * (N + 1) / 2);
   
   -- Sum multiples of M below N.
   function Sum_Multiples_Less_Than (M, N : Positive) return Positive is
     (M * Triangle_Number ((N - 1) / M));
   
   A : constant Positive := 3;
   B : constant Positive := 5;
   N : constant Positive := 1000;
   
   Result : Positive := Sum_Multiples_Less_Than (A, N)
     + Sum_Multiples_Less_Than (B, N)
     - Sum_Multiples_Less_Than (LCM (A, B), N);
   
begin
   
   Ada.Text_IO.Put_Line(Result'Image);
   
end P001;

