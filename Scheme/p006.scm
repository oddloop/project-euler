;; file:   p006.scm
;; title:  Sum square difference
;; author: Alexander Gosselin
;; e-mail: <alexandergosselin@gmail.com>
;;         <alexander.gosselin@alumni.ubc.ca>
;; date:   May 22, 2016
;;
;; link:   <https://projecteuler.net/problem=6>

;; Both the sum of squares and sum squared have a closed form solution.

;; The sum squared is the square of the trianglular number:
;;     T(n) = n(n + 1)/2
;;     T^2(n) = (n^4 + 2n^3 + n^2)/4
;;
;; The sum of squares is known as a square pyramidal number:
;;     P(n) = n(n + 1)(2n + 1)/6
;;          = (2n^3 + 3n^2 + n)/6
;;
;; The difference between these two is:
;;     SSD() = (3n^4 + 2n^3 - 3n^2 - 2*n)/12
;;           = n(3n + 2)(n - 1)(n + 1)/12

;; The prefix math syntax in scheme is both awkward and very useful,
;; since I was able to multiply four terms with a single * operator.

(define (sum-square-difference n)
  (quotient (* n (+ (* 3 n) 2) (- n 1) (+ n 1)) 12))

(define p006
  (let ((n 100))
    (sum-square-difference n)))

(display p006)
(newline)
