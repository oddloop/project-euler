;; file:   p002.scm
;; title:  Even Fibonacci numbers
;; author: Alexander Gosselin
;; e-mail: <alexandergosselin@gmail.com>
;;         <alexander.gosselin@alumni.ubc.ca>
;; date:   May 21, 2016
;;
;; link:   <https://projecteuler.net/problem=2>

(define (even-fibonaccis-up-to n)
  (define (f x0 x1)
    (let ((x2 (+ x0 (* 4 x1))))
      (if (< x2 n) 
	  (cons x2 (f x1 x2))
	  '())))
  (append '(0 2) (f 0 2)))

;; fold-left is not available by default in Gambit Scheme or scm, but it is not
;; available in MIT/GNU Scheme, so I implemented here portability.
(define (fold-left f a xs)
  (if (null? xs)
      a
      (fold-left f 
		 (f a (car xs)) 
		 (cdr xs))))

(define (sum xs)
  (fold-left + 0 xs))

(define p002
  (let ((n 4000000))
    (sum (even-fibonaccis-up-to n))))

(display p002)
(newline)
