;; file:   p004.scm
;; title:  Largest palindrome product
;; author: Alexander Gosselin
;; e-mail: <alexandergosselin@gmail.com>
;;         <alexander.gosselin@alumni.ubc.ca>
;; date:   May 22, 2016
;;
;; link:   <https://projecteuler.net/problem=4>

(define (palindrome? n)
  (let ((xs (string->list (number->string n))))
    (equal? xs (reverse xs))))

;; Returns the largest product of two d-digit numbers for which p? 
;; returns true.
(define (largest-product-d-digits p? d)
  (let ((u (- (expt 10 d) 1))  ; upper bound
		(l (expt 10 (- d 1)))) ; lower bound
    (define (f a b)
      (cond ((< a l) #f)       ; failure
			((or (> a u) (< b l))
			 (let* ((s (- a b))
					(a (- a (quotient (+ s 1) 2)))
					(b (+ b (quotient (- s 1) 2))))
			   (f a b)))
			(else (let ((c (* a b)))
					(if (p? c) c
						(f (+ a 1) (- b 1)))))))
    (f u u)))

(define p004
  (let ((d 3))
    (largest-product-d-digits palindrome? d)))

(display p004)
(newline)
