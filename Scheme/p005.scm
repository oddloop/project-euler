;; file:   p005.scm
;; title:  Smallest multiple
;; author: Alexander Gosselin
;; e-mail: <alexandergosselin@gmail.com>
;;         <alexander.gosselin@alumni.ubc.ca>
;; date:   May 22, 2016
;;
;; link:   <https://projecteuler.net/problem=5>

(define (gcd a . bs)
  (define (f a b)
    (let ((b (abs b)))
      (if (< a b) (g b a) 
	  (g a b))))
  (define (g a b)
    (if (zero? b) a
	(let ((r (remainder a b)))
	  (g b r))))
  (let ((a (abs a)))
    (if (or (null? bs) (= a 1)) a
	(apply gcd (f a (car bs)) (cdr bs)))))

(define (lcm a . bs)
  (if  (or (null? bs) (zero? a)) a
       (let* ((b (car bs))
	      (d (gcd a b))
	      (m (* (quotient a d) b)))
	 (apply lcm m (cdr bs)))))

(define (range a b)
  (if (> a b) '()
      (cons a (range (+ a 1) b))))

(define p005
  (let ((n 20))
    (apply lcm (range 2 n))))

(display p005)
(newline)
