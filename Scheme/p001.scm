;; file:   p001.scm
;; title:  Multiples of 3 and 5
;; author: Alexander Gosselin
;; e-mail: <alexandergosselin@gmail.com>
;;         <alexander.gosselin@alumni.ubc.ca>
;; date:   April 9, 2017
;; 
;; link:   <https://projecteuler.net/problem=1>

;; the nth triangle number
(define (triangle n)
  (quotient (* n (+ n 1)) 2))

;; sum of multiples of x less than n
(define (smlt x n)
  (* x (triangle (quotient (- n 1) x))))

;; sum of multiples of x or y less than n
(define (smlt2 x y n)
  (+ (smlt x n) (smlt y n) (- (smlt (lcm x y) n))))

;; the particular solution
(define p001
  (let ((a 3) (b 5) (m 1000))
    (smlt2 a b m)))

(display p001)
(newline)
