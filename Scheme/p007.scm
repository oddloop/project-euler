;; file:   p007.scm
;; title:  10001st prime
;; author: Alexander Gosselin
;; e-mail: alexandergosselin@gmail.com
;;         alexander.gosselin@alumni.ubc.ca
;; date:   May 23, 2016
;;
;; link:   <https://projecteuler.net/problem=7>

(define (prime-upper-bound n)
  (* n (log (* n (log n)))))

(define (floor->exact x)
  (inexact->exact (floor x)))

;; Returns the nth prime.
;; Based on a few tests with bash time, the compiled code runs faster
;; when mark-composites is defined inside seive.
(define (prime-eratosthenes n)
  (if (< n 6) (vector-ref '#(2 3 5 7 11) (- n 1))
      (let* ((m (quotient (floor->exact (prime-upper-bound n)) 2))
	     (is-prime (make-vector m #t)))
	(define (seive i c)
	  (if (vector-ref is-prime i) 
	      (let ((p (+ (* 2 i) 1))
		    (c (+ c 1)))
		(define (mark-composites j)
		  (if (>= j m) #f
		      (begin
			(vector-set! is-prime j #f)
			(mark-composites (+ j p)))))
		(if (= c n) p
		    (begin
		      (mark-composites (* i (+ p 1)))
		      (seive (+ i 1) c))))
	      (seive (+ i 1) c)))
	(seive 1 1))))

(define p007
  (let ((n 10001))
    (prime-eratosthenes n)))

(display p007)
(newline)
