;; file:   p003.scm
;; title:  Largest prime factor
;; author: Alexander Gosselin
;; e-mail: <alexandergosselin@gmail.com>
;;         <alexander.gosselin@alumni.ubc.ca>
;; date:   May 22, 2016
;;
;; link:   <https://projecteuler.net/problem=3>

(define (divides? a b)
  (if (= (modulo b a) 0)
      #t
      #f))

(define (trial-division n)
  (define (f k n r)
    (if (> k r)
	(cons n '())
	(if (divides? k n)
	    (let* ((q (quotient n k))
		   (r (sqrt q)))
	      (cons k (f k q r)))
	    (f (+ k 2) n r))))
  (define (g n)
    (if (even? n)
	(if (= n 2)
	    (cons 2 '())
	    (cons 2 (g (quotient n 2))))
	(f 3 n (sqrt n))))
  (if (<= n 1)
      '()
      (g n)))

;; Since the trial division function above returns a sorted list of 
;; factors, we know that the last element is the largest factor.
(define (last xs)
  (let ((xt (cdr xs)))
    (if (null? xt)
	(car xs)
	(last xt))))

(define p003
  (let ((n 600851475143))
    (last (trial-division n))))

(display p003)
(newline)
