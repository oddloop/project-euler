! file:   p001.f90
! title:  Multiples of 3 and 5
! author: Alexander Gosselin
! e-mail: <alexandergosselin@gmail.com>
! date:   May 8, 2015
!
! link:   <https://projecteuler.net/problem=1>

function sum_divisible_by(x, n) result(y)
  integer :: x ! input
  integer :: y ! output
  integer :: d
  d = n/x
  y = x * (d * (d + 1))/2
end function sum_divisible_by

program p001
  integer :: n = 999
  integer :: sum_divisible_by
  print '(I0)', sum_divisible_by(3, n) + sum_divisible_by(5, n) &
                - sum_divisible_by(15, n)
end program p001
