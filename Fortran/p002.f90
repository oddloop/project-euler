! file:   pe_002.f90
! title:  Even Fibbonacci numbers
! author: Alexander Gosselin
! e-mail: <alexandergosselin@gmail.com>
! date:   May 8, 2015
! 
! link:   <https://projecteuler.net/problem=2>

program p002
  integer :: max = 4000000
  integer :: prev = 0, curr = 2, next = 0
  integer :: sum = 0
  do while (next <= max)
    sum = sum + curr
    next = prev + 4*curr
    prev = curr
    curr = next
  end do
  print '(I0)', sum
end program p002
