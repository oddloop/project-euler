{-|
file:   p019.hs
title:  Counting sundays
author: Alexander Gosselin
e-mail: <alex@oddloop.ca>
date:   November 29, 2018

link:   <https://projecteuler.net/problem=19>
-}

import Data.Time.Calendar
import Data.Time.Calendar.OrdinalDate

-- | Blackbird combinator
(...) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(...) = (.) . (.)

-- The nearest first of the month on or after the argument. 
nearestFirst :: Day -> Day
nearestFirst day = case (toGregorian day) of
  (_, _, 1) -> day
  _         -> addGregorianMonthsClip 1 day

-- All first of months starting with the one nearest the argument.
firstsFrom :: Day -> [Day]
firstsFrom = iterate (addGregorianMonthsClip 1) . nearestFirst

isSunday :: Day -> Bool
isSunday = (== 7) . snd . mondayStartWeek
-- isSunday = (== 1) . snd . sundayStartWeek (incorrect result)

sundayFirstsFrom :: Day -> [Day]
sundayFirstsFrom = filter isSunday . firstsFrom

countSundayFirstsBetween :: Day -> Day -> Int
countSundayFirstsBetween start end =
  length . takeWhile (< end) . sundayFirstsFrom $ start

p019 :: Int
p019 = countSundayFirstsBetween start end
  where
    start = fromGregorian 1901 1  1
    end   = fromGregorian 2000 12 31

main :: IO ()
main = do
  print p019
