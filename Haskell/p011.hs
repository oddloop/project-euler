{-|
file:   p011.hs
title:  Largest product in a grid
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
        <alexander.gosselin@alumni.ubc.ca>
date:   May 24, 2016

link:   <https://projecteuler.net/problem=11>
-}

import Data.Char (digitToInt, isSpace)

-- This Queue implementation is copied from Rafal Szymanski's blog.
-- <http://rafal.io/posts/haskell-queues.html>

data Queue a = Queue {
  inbox  :: [a],
  outbox :: [a]
} deriving (Eq, Show)

push :: a -> Queue a -> Queue a
push x (Queue xs ys) = Queue (x:xs) ys

pop :: Queue a -> (a, Queue a)
pop (Queue xs (y:ys)) = (y, Queue xs ys)
pop (Queue xs [])     = pop (Queue [] (reverse xs))

-- This is the same function used in my solution to problem 8.
largestSubseriesProduct :: Int -> [Int] -> Int
largestSubseriesProduct n = f 0 0 (Queue [] []) 
  where
    f :: Int -> Int -> Queue Int -> [Int] -> Int
    f m _ _ []     = m
    f m _ _ (0:xs) = f m 0 (Queue [] []) xs
    f m s q xs@(x:xt)
      | s == n    = g (max m p) p q xs
      | otherwise = f m (s + 1) (push x q) xt
      where
        p = product (inbox q)
        
    g :: Int -> Int -> Queue Int -> [Int] -> Int
    g m _ _ [] = m
    g m _ _ (0:xs) = f m 0 (Queue [] []) xs
    g m p q (x:xs) = g (max m p') p' q' xs
      where
        p' = x * (quot p y)
        (y, q') = pop (push x q)

transpose :: [[a]] -> [[a]]
transpose []         = []
transpose xs@([_]:_) = concat xs : []
transpose xs         = map head xs : transpose (map tail xs)

-- Version of zipWith that is not right lazy.
zipWith' :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWith' _  _      []     = []
zipWith' f  []     ys     = ys
zipWith' f  (x:xs) (y:ys) = f x y : zipWith' f xs ys

-- This version reverses each of the diagonals.
-- A non-reversing version:
-- diagonals (x:xs) = map reverse
--                  $ foldl f (map (:[]) x) xs
diagonals :: [[a]] -> [[a]]
diagonals (x:xs) = foldl f (map (:[]) x) xs
  where
    f ys (z:zs) = [z] : zipWith' (:) zs ys

p011 :: String -> Int
p011 = maximum
     . map (largestSubseriesProduct n)
     . concat
     . (\xs -> [xs, transpose xs, diagonals xs, diagonals (reverse xs)])
     . map (map read . words)
     . lines
  where
    n = 4

main :: IO ()
main = do
  contents <- readFile "../p011.txt"
  print (p011 contents)
