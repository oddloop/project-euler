{- |
file:   p003.hs
title:  Largest prime factor
author: Alexander P. Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   September 20, 2014

link:   <https://projecteuler.net/problem=3>
-}

largestPrimeFactor :: Integer -> Integer -> Integer
largestPrimeFactor x y
    | x == y        = x
    | mod x y == 0  = largestPrimeFactor (quot x y) y
    | otherwise     = largestPrimeFactor x (y + 2)

p003 :: Integer
p003 = largestPrimeFactor 600851475143 3

main :: IO ()
main = do
    print p003
