{-|
file:   p006.hs
title:  Sum square difference
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
        <alex@oddloop.ca>
date:   November 22, 2018

link:   <https://projecteuler.net/problem=6>
-}

triangle :: Integral a => a -> a
triangle n = (n*(n + 1)) `div` 2

squarePyramid :: Integral a => a -> a
squarePyramid n = (n*(n + 1)*(2*n + 1)) `div` 6

sumSquareDifference :: Integral a => a -> a
sumSquareDifference n = (triangle n)^2 - squarePyramid n

p006 :: Integer
p006 = sumSquareDifference n
  where
    n = 100

main :: IO ()
main = do
  print p006
