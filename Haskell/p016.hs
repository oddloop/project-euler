{-
file:   p016.hs
title:  Power digit sum
author: Alexander P. Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   May 30, 2015

link:   <https://projecteuler.net/problem=16>
-}

import Data.Char (digitToInt)

p016 = sum $ map digitToInt $ show (2^1000)

main :: IO ()
main = do
    print p016

