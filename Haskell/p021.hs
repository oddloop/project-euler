{-|
file:   p021.hs
title:  Amicable Numbers
author: Alexander Gosselin
e-mail: <alex@oddloop.ca>
date:   November 29, 2018

link:   <https://projecteuler.net/problem=21>

note:   The functions defined in this file are not total functions; they do not
        gracefully handle negative numbers or empty lists. Do not use them unmodified
        in any kind of production code.
-}

import Data.Array (Array, (!))
import qualified Data.Array as Array

import Data.Function (fix)

iSqrt :: Integral a => a -> a
iSqrt 0 = 0
iSqrt 1 = 1
iSqrt n = go (n `quot` 2)
  where
    go x = if x' >= x then x else go x'
      where
        x' = (x + (n `quot` x)) `quot` 2

-- An list of divisors of n obtained by trial division; suitable for small n.
divisors :: Integral a => a -> [a]
divisors n = f . filter ((== 0) . (n `rem`)) $ [1..r]
  where
    r = iSqrt n
    f = case (r^2 == n) of
      True  -> (++) <*> map (n `quot`) . tail . reverse
      False -> (++) <*> map (n `quot`) . reverse
  
properDivisors :: Integral a => a -> [a]
properDivisors = init . divisors

-- Sum of proper divisors of n.
divisorSum :: Integral a => a -> a
divisorSum = sum . properDivisors

-- A list of unique amicable pairs where both elements are less than n.
amicablePairsLessThan :: Int -> [(Int, Int)]
amicablePairsLessThan n = filter amicable divisorSumPairs
  where
    divisorSumPairs = (0, 0) : (zip <*> map divisorSum $ [1..n - 1])
    divisorSumArray = Array.array (0, n - 1) divisorSumPairs
    amicable (a, b) = a < b && b < n && a == divisorSumArray ! b

-- A list of amicable numbers drawn from pairs where both elements are less than n.
amicablesLessThan :: Int -> [Int]
amicablesLessThan = uncurry (++) . unzip . amicablePairsLessThan 

p021 :: Int
p021 = sum (amicablesLessThan n)
  where
    n = 10000

main :: IO ()
main = do
  print p021
