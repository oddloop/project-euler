{-|
file:   p015.hs
title:  Lattice paths
author: Alexander P. Gosselin
e-mail: alexandergosselin@gmail.com
date:   October 28, 2015

link:   <https://projecteuler.net/problem=15>
-}

factorial :: (Enum a, Integral a) => a -> a
factorial n
  | n < 0     = undefined
  | n == 0    = 1
  | n == 1    = 1
  | otherwise = product [2..n]

fallingFactorial :: (Enum a, Integral a) => a -> a -> a
fallingFactorial x n
  | n < 0     = undefined
  | x < n     = undefined
  | x == n    = factorial x
  | otherwise = product [x - n + 1..x]

binomial :: (Enum a, Integral a) => a -> a -> a
binomial n k
  | n < k       = undefined
  | k == 0      = 1
  | k > (n - k) = binomial n (n - k)
  | otherwise   = fallingFactorial n k `quot` factorial k

latticePaths :: (Enum a, Integral a) => a -> a 
latticePaths n = binomial (2*n) n

p015 :: Integer
p015 = latticePaths n
  where
    n = 20

main :: IO ()
main = do
  print p015
