{-|
file:   p013.hs
title:  Large sum
author: Alexander P. Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   May 30, 2015

link:   <https://projecteuler.net/problem=13>

note 1:  Since there are 100 numbers to add, digits in the twelfth
  position will affect the value of the 10th digit of the sum, but
  digits after the 12th digit do not affect any of the first 10
  digits of the sum, so we can ignore them.
          
note 2:  When summing over 100 12-digit numbers, we get a 14-digit
  number. We can divide this by 10000 to shave off the last 4 digits.
-}

p013 :: String -> Integer
p013 = (flip quot 10000)
       . sum 
       . map read
       . map (take 12) 
       . words

main :: IO ()
main = do
    contents <- readFile "../p013.txt"
    print (p013 contents)
