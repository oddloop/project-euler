{-|
file:   p018.hs
title:  Maximum path sum I
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   November 28, 2018

link:   <https://projecteuler.net/problem=18>
-}

pairwiseMax :: Ord a => [a] -> [a]
pairwiseMax = zipWith max <*> tail

-- | D combinator - dove.
-- <http://hackage.haskell.org/package/data-aviary-0.4.0>
dove :: (a -> c -> d) -> a -> (b -> c) -> b -> d
dove f x g y = f x (g y)

pathSum :: (Num a, Ord a) => [[a]] -> [a]
pathSum = foldr1 (flip (dove (zipWith (+))) pairwiseMax)

maximumPathSum :: (Num a, Ord a) => [[a]] -> a
maximumPathSum = maximum . pathSum

readGrid :: Read a => String -> [[a]]
readGrid = map (map read) . map words . lines

p018 :: IO Int
p018 = maximumPathSum . readGrid <$> readFile "../p018.txt"
  
main :: IO ()
main = print =<< p018
