{-
file:   p024.hs
title:  Lexicographic permutations
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   May 25, 2015

link:   <https://projecteuler.net/problem=24>
-}

-- Memoized factorial function    
factorial :: Int -> Int
factorial = (map factorial' [0..] !!)
  where
    factorial' 0 = 1
    factorial' n = n * factorial (n - 1)

-- Permute a finite list n times
permutation :: Eq a => Int -> [a] -> [a]
permutation 0 xs = xs -- 1st permutation in Project Euler convention
permutation n xs = x : permutation n' xs'
  where
    x   = xs !! (quot n f)
    n'  = mod n f
    xs' = filter (/= x) xs
    f   = factorial (length xs - 1)

-- The millionth permutation of a list is permutation number 999999
p024 :: String
p024 = permutation (n - 1) "0123456789"
  where
    n = 1000000

main :: IO ()
main = do
    putStrLn p024

