{-|
file:   pe_005.hs
title:  Smallest multiple
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
        <alexander.gosselin@alumni.ubc.ca>
date:   February 7, 2016

link:   <https://projecteuler.net/problem=5>
-}

allWhile :: (a -> Bool) -> (a -> Bool) -> [a] -> Bool
allWhile _ _ [] = True
allWhile f g (x:xs)
    | not (f x) = False
    | g x       = allWhile f g xs
    | otherwise = True

-- Trial division prime generator, only suitable for small primes. 
primes :: Integral a => [a]
primes = 2:[ p | p <- [3, 5..], 
    allWhile (\q -> rem p q /= 0) (\q -> q^2 <= p) primes ]

largestPowerLessThan :: (Ord a, Num a) => a -> a -> a
largestPowerLessThan n x = last 
    $ takeWhile (< n) 
    $ iterate (*x) 1

-- The smallest number divisible by 2 through n is the product of the
-- largest powers of primes less than n. 
smallestMultiple :: Integral a => a -> a
smallestMultiple n = product 
    $ map (largestPowerLessThan (n + 1))
    $ takeWhile (< n) primes

p005 :: Int
p005 = smallestMultiple 20

main :: IO ()
main = do
  print p005

