{- |
file:   p002.hs
title:  Even Fibonacci numbers
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   September 21, 2014

link:   <https://projecteuler.net/problem=2>
-}

evenFibonaccis :: [Integer]
evenFibonaccis = 
    0 : 2 : zipWith (+) evenFibonaccis (map (* 4) (tail evenFibonaccis))

p002 :: Integer
p002 = sum . takeWhile (< n) $ evenFibonaccis
  where
    n = 4000000

main :: IO ()
main = do
    print p002
