{-|
file:   p008.hs
title:  Largest product in a series
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
        <alexander.gosselin@alumni.ubc.ca>
date:   May 23, 2016

link:   <https://projecteuler.net/problem=8>
-}

-- This Queue implementation is copied from Rafal Szymanski's blog.
-- <http://rafal.io/posts/haskell-queues.html>

import Control.Monad (liftM)
import Data.Char (digitToInt, isSpace)

data Queue a = Queue {
  inbox  :: [a],
  outbox :: [a]
} deriving (Eq, Show) -- derive foldable

push :: a -> Queue a -> Queue a
push x (Queue xs ys) = Queue (x:xs) ys

pop :: Queue a -> (a, Queue a)
pop (Queue xs (y:ys)) = (y, Queue xs ys)
pop (Queue xs [])     = pop (Queue [] (reverse xs))

largestSubseriesProduct :: Int -> [Int] -> Int
largestSubseriesProduct n = f 0 0 (Queue [] []) 
  where
    f :: Int -> Int -> Queue Int -> [Int] -> Int
    f m _ _ []     = m
    f m _ _ (0:xs) = f m 0 (Queue [] []) xs
    f m s q xs@(x:xt)
      | s == n    = g (max m p) p q xs
      | otherwise = f m (s + 1) (push x q) xt
      where
        p = product (inbox q)
        
    g :: Int -> Int -> Queue Int -> [Int] -> Int
    g m _ _ [] = m
    g m _ _ (0:xs) = f m 0 (Queue [] []) xs
    g m p q (x:xs) = g (max m p') p' q' xs
      where
        p' = x * (quot p y)
        (y, q') = pop (push x q)

p008 :: String -> Int
p008 = largestSubseriesProduct n
     . map digitToInt
     . filter (not . isSpace)
  where
    n = 13

main :: IO ()
main = do
  contents <- readFile "../p008.txt"
  print (p008 contents)
