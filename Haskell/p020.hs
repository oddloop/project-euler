{-|
file:   p020.hs
title:  Number letter counts
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   May 24, 2015

link:   <https://projecteuler.net/problem=20>
-}

import Data.Char (digitToInt)

factorial :: Integer -> Integer
factorial 0 = 1
factorial 1 = 1
factorial n = n * factorial (n - 1)

factorialDigitSum :: Integer -> Int
factorialDigitSum = sum . (map digitToInt) . show . factorial

p020 = factorialDigitSum 100

main :: IO ()
main = do
    print p020
