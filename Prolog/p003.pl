/**
 * file:   p003.pl
 * title:  Largest prime factor
 * author: <alexandergosselin@gmail.com>
 *         <alex@oddloop.ca>
 * date:   November 5, 2019
 *
 * link:   <https://projecteuler.net/problem=3> 
 */

:- use_module(library(clpfd)).

largest_prime_factor(N, X) :-
    prime_factors(N, Xs),
    last(Xs, X), !.

prime_factors(N, Xs) :-
    N in 1..sup,
    prime_factors(N, 2, Xs) ->
	product(Xs, N).

prime_factors(1, _, []).

prime_factors(N, X, [X|Xs]) :-
    0 #= N mod X,
    N1 #= N div X,
    prime_factors(N1, X, Xs).

prime_factors(N, 2, Xs) :-
    prime_factors(N, 3, Xs).

prime_factors(N, X, Xs) :-
    X1 #= X + 2,
    prime_factors(N, X1, Xs).

product([], 1).

product([X|Xs], N) :-
    product(Xs, N1),
    N #= X * N1.

%% p003(X) :-
%%     N #= 13195,
%%     largest_prime_factor(N, X).

p003(X) :-
    N #= 600851475143,
    largest_prime_factor(N, X).

:- initialization(main).
main :-
    p003(X),
    write(X), nl,
    halt.
