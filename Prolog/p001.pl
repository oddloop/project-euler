/**
 * file:   p001.pl
 * title:  Multiples of 3 and 5
 * author: Alexander Gosselin
 * e-mail: <alexandergosselin@gmail.com>
 *         <alexander.gosselin@alumni.ubc.ca>
 * date:   May 31, 2016
 *
 * link:   <https://projecteuler.net/problem=1>
 */

triangle_number(N, X) :- X is (N*(N + 1))//2.

multiples_less_than(A, N, X) :- X is (N - 1)//A.

sum_multiples_less_than(A, N, X) :-
    multiples_less_than(A, N, M),
    triangle_number(M, TM),
    X is A * TM.

p001(X) :- 
    A = 3, B = 5, N = 1000,
    AB is A * B,
    sum_multiples_less_than(A, N, XA),
    sum_multiples_less_than(B, N, XB),
    sum_multiples_less_than(AB, N, XAB),
    X is XA + XB - XAB.

:- initialization(main).
main :- 
    p001(X),
    write(X), 
    nl, halt.
