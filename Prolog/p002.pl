/**
 * file:   p002.pl
 * title:  Even Fibonacci numbers
 * author: <alexandergosselin@gmail.com>
 *         <alex@oddloop.ca>
 * date:   November 21, 2018
 *
 * link:   <https://projecteuler.net/problem=2> 
 */

sum_even_fibonaccis_less_than(N, X) :-
    sum_even_fibonaccis_less_than(N, 0, 2, 0, X).

sum_even_fibonaccis_less_than(N, _, F1, X, X) :-
    F1 > N.

sum_even_fibonaccis_less_than(N, F0, F1, S1, X) :-
    F2 is F0 + 4*F1,
    S2 is S1 + F1,
    sum_even_fibonaccis_less_than(N, F1, F2, S2, X).
    
p002(X) :-
    N is 4000000,
    sum_even_fibonaccis_less_than(N, X).

:- initialization(main).
main :-
    p002(X),
    write(X),
    nl, halt.
