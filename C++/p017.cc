/*
 * file:   p017.cc
 * title:  Number letter counts
 * author: Alexander P. Gosselin
 * e-mail: <alexandergosselin@gmail.com>
 * date:   November 28, 2018
 *
 * link:   <https://projecteuler.net/problem=17>
 */

#include <iostream>

// zero, one, two, three, four, five, six, seven, eight, nine
const int ONES_LETTERS[10] = {0, 3, 3, 5, 4, 4, 3, 5, 5, 4};
// ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen,
// eighteen, nineteen
const int TEEN_LETTERS[10] = {3, 6, 6, 8, 8, 7, 7, 9, 8, 8};
// ones, teens, twenty, thirty, forty, fifty sixty, seventy, eighty, ninety 
const int TENS_LETTERS[10] = {0, 0, 6, 6, 5, 5, 5, 7, 6, 6};
const int HUNDRED_LETTERS  = 7;
const int AND_LETTERS = 3;
const int ONE_THOUSAND_LETTERS = 11;

int main() {
  int one_to_nine = 0;
  int ten_to_nineteen = 0;
  for (int ones = 0; ones < 10; ones++) {
    one_to_nine += ONES_LETTERS[ones];
    ten_to_nineteen += TEEN_LETTERS[ones];
  }
  int one_to_ninety_nine = one_to_nine + ten_to_nineteen;
  for (int tens = 2; tens < 10; tens++) {
    one_to_ninety_nine += 10*TENS_LETTERS[tens] + one_to_nine;
  }
  int one_to_nine_hundred_and_ninety_nine = one_to_ninety_nine;
  for (int hundreds = 1; hundreds < 10; hundreds++) {
    one_to_nine_hundred_and_ninety_nine +=
      100*(ONES_LETTERS[hundreds] + HUNDRED_LETTERS)
      + 99*AND_LETTERS + one_to_ninety_nine;
  }
  int letter_count = one_to_nine_hundred_and_ninety_nine
    + ONE_THOUSAND_LETTERS;
  std::cout << letter_count << std::endl;
  return 0;
} 
