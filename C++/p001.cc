/*
 * file:    p001.cc
 * title:   Multiples of 3 and 5
 * author:  Alexander Gosselin
 * e-mail:  <alexandergosselin@gmail.com>
 *          <alexander.gosselin@alumni.ubc.ca>
 * date:    May 8, 2015
 * 
 * link:    <https://projecteuler.net/problem=1>
 */

#include <iostream>
using namespace std;

static const int N = 999;

int sum_divisible_by(int x);

int main() {
  cout << sum_divisible_by(3) + sum_divisible_by(5) 
          - sum_divisible_by(15) << endl;
  return 0;
}

int sum_divisible_by(int x) {
  int d = N/x;
  return x * (d * (d + 1))/2;
}
