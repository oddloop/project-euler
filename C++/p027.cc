/*
file:   p027_new.cc
title:  Quadratic primes
author: Alexander Gosselin
email:  <alexandergosselin@gmail.com>
date:   November 29, 2018

link:   <https://projecteuler.net/problem=27>
*/

#include <algorithm>
#include <iostream>
#include <vector>

const int64_t MAX_ABS_A = 999;
const int64_t MAX_ABS_B = 1000;

int64_t isqrt(int64_t n) {
  if (n == 0 || n == 1) {
    return n;
  }
  int64_t x = n/2;
  for (int64_t y; ; x = y) {
    y = (x + (n/x))/2;
    if (y >= x) {
      break;
    }
  }
  return x;
}

void expand_sieve(int64_t n, std::vector<bool> &sieve) {
  int64_t i = sieve.size();
  while (sieve.size() < (n + 1)/2) {
    sieve.push_back(true);
  }
  for (int64_t j = 1; j < isqrt(sieve.size()/2) + 1; j++) {
    if (sieve[j]) {
      int64_t p = 2*j + 1;
      int64_t k = std::min(p*j + j, i + p - (i - j)%p);
      while (k < sieve.size()) {
	sieve[k] = false;
	k += p;
      }
    }
  }
}

bool is_prime(int64_t n, std::vector<bool> &sieve) {
  if (n < 0) {
    return false;
  }
  if (n % 2 == 0) {
    if (n == 2) {
      return true;
    }
    return false;
  }
  if (sieve.size() < (n + 1)/2) {
    expand_sieve(n, sieve);
  }
  return sieve[n/2];
}

int64_t prime_sequence_length(int64_t a, int64_t b, std::vector<bool> &sieve) {
  int64_t l = 0;
  int64_t n = 0;
  while (is_prime(n*n + a*n + b, sieve)) {
    l++;
    n++;
  }
  //std::cout << a << ", " << b << ", " << l << std::endl;
  return l;
}

int64_t p027() {
  std::vector<bool> sieve = {false, true}; // 1 is not prime; 3 is.
  int64_t product_ab;
  int64_t max_l = 0;
  for (int64_t a = -MAX_ABS_A | 1; a <= MAX_ABS_A; a += 2) {
    for (int64_t b = std::max(-MAX_ABS_B | 1, -a); b <= MAX_ABS_B; b += 2) {
      int64_t l = prime_sequence_length(a, b, sieve);
      if (l > max_l) {
	max_l = l;
	product_ab = a*b;
      }
    }
  }
  return product_ab;
}

int main() {
  std::cout << p027() << std::endl;
  return 0;
}

