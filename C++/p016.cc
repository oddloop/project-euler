/*
 * file:   p016.cc
 * title:  Power digit sum
 * author: Alexander Gosselin
 * e-mail: <alexandergosselin@gmail.com>
 * date:   October 6, 2014
 * 
 * link:   <https://projecteuler.net/problem=16>
 * 
 * note:   Very large powers of two (<= 2^2047) can be stored as
 *         exact values in double precision floating points.
 */

#include <cmath>
#include <iostream>
using namespace std;

int main() {
  int digit_sum = 0;
  double power = 0;
  power = pow(2, 1000);
  string digits = to_string(power);
  for (char digit : digits) {
    if (digit == '.') break;
    digit_sum += digit - '0';
  }
  cout << digit_sum << endl;
  return 0;
}
