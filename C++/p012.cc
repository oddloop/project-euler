/*
file:   p012.cc
title:  Highly divisible triangular number 
author: Alexander Gosselin
email:  <alex@oddloop.ca>
date:   November 28, 2018

link:   <https://projecteuler.net/problem=12>
*/

#include <iostream>

const uint64_t N = 500;

uint64_t count_divisors(uint64_t n) {
  // count the number of divisors by trial division
  uint64_t divisor_count = 0;
  uint64_t i;
  for (i = 1; i*i < n; i++) {
    if (n % i == 0) {
      divisor_count += 2;
    }
  }
  if (i*i == n) {
    divisor_count++;
  }
  return divisor_count;
}

uint64_t highly_divisible_triangular_number(uint64_t n) {
  // return the first triangular number with more than n divisors
  // linear search
  uint64_t triangular_number = 0;
  for (uint64_t i = 1; ; i++) {
    triangular_number += i;
    if (count_divisors(triangular_number) > n) {
      return triangular_number;
    }
  }
}

uint64_t p012() {
  return highly_divisible_triangular_number(N);
}

int main() {
  std::cout << p012() << std::endl;
  return 0;
}
