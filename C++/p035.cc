/*
 * file:    p035.cc
 * title:   Circular primes
 * author:  Alexander Gosselin
 * e-mail:  <alexandergosselin@gmail.com>
 * date:    December 18, 2013
 *
 * link:    <https://projecteuler.net/problem=35>
 */

#include <algorithm> 
#include <bitset>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

const size_t primes_up_to = 1000000;

int main() {
  //generate list of primes suing sieve of Eratosthenes
  bitset<primes_up_to> is_prime;
  is_prime.set();
  is_prime[0] = false;
  is_prime[1] = false;
  for(size_t number = 2; number < primes_up_to; number++) {
    if(is_prime[number]) {
      for(size_t composite = 2*number; composite < primes_up_to; 
          composite += number) {
        is_prime[composite] = false;
      }
    }
  }
  //check primes for circularity
  bitset<primes_up_to> checked_for_circularity;
  size_t number_of_circular_primes = 0;
  for(size_t number = 0; number < primes_up_to; number++) {
    if(checked_for_circularity[number]) {continue;}
    checked_for_circularity[number] = true;
    if(is_prime[number]) {
      string prime_string = to_string(number);
      string rotated_string = prime_string;
      vector<size_t> circular_primes;
      for(size_t rotation = 1; rotation < prime_string.length(); rotation++) {
        rotate(rotated_string.begin(),rotated_string.begin()+1,
               rotated_string.end());
        size_t rotated_number = stoi(rotated_string);
        if(is_prime[rotated_number]) {
          circular_primes.push_back(rotated_number);
        }
      }
      if(circular_primes.size() >= prime_string.length()-1) {
        number_of_circular_primes++;
        for(auto prime : circular_primes) {
          if(prime != number) {
            checked_for_circularity[prime] = true;
            number_of_circular_primes++;
          }
        }
      }
    }
  }
  cout << number_of_circular_primes << endl;
}
