/*
 * file:    p036.cc
 * title:   Double-base palindromes
 * author:  Alexander P. Gosselin
 * e-mail:  <alexandergosselin@gmail.com>
 * date:    October 2, 2014
 * 
 * link:    <https://projecteuler.net/problem=36>
 */

#include <iostream>
using namespace std;

int reverse_binary(int i);
bool is_decimal_palindrome(int i);
bool is_binary_palindrome(int i);

void test (int i);

const int N = 1000000;

int main() {
  // even numbers cannot be binary palindromes, since according to the
  // problem specification, palndromic numbers may not have leading 0s
  uint64_t sum = 0;
  for (int i = 1; i < N; i += 2) {
    if (is_binary_palindrome(i) && is_decimal_palindrome(i)) {
      sum += i;
    }
  }
  cout << sum << endl;
  return 0;
}

int reverse_binary(int i) {
  int r = 0;
  while (i > 0) {
    r <<= 1;
    r |= i & 1;
    i >>= 1;
  }
  return r ;
}

bool is_decimal_palindrome(int i) {
  string is = to_string(i);
  for (struct {string::iterator f; string::reverse_iterator r;} it = 
       {is.begin(), is.rbegin()}; 
       size_t(it.f - is.begin()) <= is.size(); ++it.f, ++it.r) {
    if (*(it.f) != *(it.r)) return false;
  }
  return true;
}

bool is_binary_palindrome(int i) {
  if (i == reverse_binary(i)) {
    return true;
  } else {
    return false;
  }
}
