/*
 * file:    p033.cc
 * title:   Digit cancelling fractions
 * author:  Alexander Gosselin
 * e-mail:  <alexandergosselin@gmail.com>
 * date:    December 15, 2013
 *
 * link:    <https://projecteuler.net/problem=33>
 */

#include <iostream>
using namespace std;

int main() {
  size_t numerator_product = 1;
  size_t denominator_product = 1;
  for(size_t n = 1; n < 10; n++) {
    for(size_t d = n+1; d < 10; d++) {
      for(size_t i = d+1; i < 10; i++) {
        if(9*n*(i-d)==i*(d-n)) {
          numerator_product *= n;
          denominator_product *= d;
        }
      }
    }
  }
  for(size_t common_denominator = numerator_product; 
      common_denominator > 1; common_denominator--){
    if(denominator_product%common_denominator == 0) {
      cout << denominator_product/common_denominator << endl;
      break;
    }
  }
  return 0;
}
