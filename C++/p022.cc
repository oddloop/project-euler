/*
 * file:   p022.cc
 * title:  Names scores
 * author: Alexander P. Gosselin
 * e-mail: alexandergosselin@gmail.com
 * date:   <September 15, 2013>
 *
 * link:   <https://projecteuler.net/problem=22>
 */

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, char **argv)
{
  //import names.txt into vector of strings
  ifstream ifs ("../p022_names.txt");
  vector<string> names;
  while(ifs.peek() != EOF) {
    if(ifs.get() == '"') {
      //initialize new name
      string new_name;
      //read name from list
      while(ifs.peek() != '"') {
      new_name.push_back(ifs.get());
      }
      ifs.get();
      //insert name into sorted vector of names
      bool placed = false;
      for(size_t index = 0; index < names.size(); index++) {
        if(names[index].compare(new_name) > 0){
          names.insert(names.begin()+index, new_name);
          placed = true;
          break;
        }
      }
      if(placed == false) names.push_back(new_name);
    }
  }
  //calculate name score for each name
  unsigned int name_score_sum = 0;
  for(size_t index = 0; index < names.size(); index++) {
    size_t name_score = 0;
    for(char &letter : names[index]) {
      name_score += (int)letter-'A'+1;
    }
    name_score_sum += name_score*(index+1);
  }
  cout << name_score_sum << endl;
  return 0;
}
