/*
 * file:   p006.cc
 * title:  Sum square difference
 * author: Alexander Gosselin
 * e-mail: <alexandergosselin@gmail.com>
 * date:   October 4, 2014
 * 
 * link:   <https://projecteuler.net/problem=6>
 */

#include <iostream>
using namespace std;

const int N = 100;

int main () {
  int sum_square_difference = (N*(N*N - 1)*(3*N + 2))/12;
  cout << sum_square_difference << endl;
  return 0;
}
