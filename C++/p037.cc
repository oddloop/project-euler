/*
file:   p037.cc
title:  Truncatable Primes
author: Alexander Gosselin
email:  <alex@oddloop.ca>
date:   November 30, 2018
        December 1, 2018

link:   <https://projecteuler.net/problem=37>

references:
    On Truncatable Primes - I. O. Angell and H. J. Godwin (1977)
    <http://www.ams.org/journals/mcom/1977-31-137/S0025-5718-1977-0427213-2/>
*/

#include <iostream>
#include <map>
#include <numeric>
#include <vector>

int isqrt(int n) {
  // integer square root
  if (n == 0 || n == 1) {
    return n;
  }
  int x = n/2;
  for (int y; ; x = y) {
    y = (x + (n/x))/2;
    if (y >= x) {
      break;
    }
  }
  return x;
}


bool is_prime(int n, std::map<int, bool> &cache) {
  // trial division with memoization
  // suitable for repeated queries on small n
  if (n % 2 == 0) {
    return n == 2;
  }
  try { // check the memo map before doing trial division
    return cache.at(n);
  }
  catch (const std::out_of_range &oor) {}
  for (int i = 3; i <= isqrt(n); i++) {
    if (n % i == 0) {
      cache[n] = false;
      return false;
    }
  }
  cache[n] = true;
  return true;
}


int ipow(int n, int k) {
  // integer exponentiation by squaring
  if (k == 0) {
    return 1;
  }
  int x = 1;
  while (k > 0) {
    if (k & 1) { // e is odd
      x *= n;
    }
    n *= n;
    k /= 2;
  }
  return x;
}


int ilog10(int n) {
  // integer base-10 logarithm
  // <https://helloacm.com/fast-integer-log10/>
  return (n >= 1000000000) ? 9 : (n >= 100000000) ? 8 : 
    (n >= 10000000) ? 7 : (n >= 1000000) ? 6 : 
    (n >= 100000) ? 5 : (n >= 10000) ? 4 :
    (n >= 1000) ? 3 : (n >= 100) ? 2 : (n >= 10) ? 1 : 0; 
}


bool is_left_truncatable_prime(int n, std::map<int, bool> &cache) {
  // return true if n is a two-sided prime, otherwise false (unused)
  int m = ipow(10, ilog10(n));
  while (is_prime(n, cache)) {
    n %= m;
    m /= 10;
  }
  return n == 0;
}
  

bool is_right_truncatable_prime(int n, std::map<int, bool> &cache) {
  // return true if n is a right-truncatable prime, otherwise false
  while (is_prime(n, cache)) {
    n /= 10;
  }
  return n == 0;
}


bool is_two_sided_prime(int n, std::map<int, bool> &cache) {
  // return true if n is a two-sided prime, otherwise false (unused)
  return is_left_truncatable_prime(n, cache)
         && is_right_truncatable_prime(n, cache);
}

int concatenate_decimal_digits(int a, int b) {
  // concatenate the decimal digits of a and b
  return a * ipow(10, ilog10(b) + 1) + b;
}

int p037() {
  // generate all right-truncatable primes and filter for two-sided primes
  std::map<int, bool> is_prime_cache = {{1, false}, {2, true}};
  std::vector<int> right_truncatable_primes = {2, 3, 5, 7};
  std::vector<int> two_sided_primes;
  for (int i = 0; i < right_truncatable_primes.size(); i++) {
    int p = right_truncatable_primes[i];
    for (int d : {1, 3, 7, 9}) {
      int q = concatenate_decimal_digits(p, d);
      if (is_prime(q, is_prime_cache)) {
	right_truncatable_primes.push_back(q);
	if (is_left_truncatable_prime(q, is_prime_cache)) {
	  two_sided_primes.push_back(q);
	}
      }
    }
  }
  // return the sum of two-sided primes (excluding single-digit primes)
  return std::accumulate(two_sided_primes.begin(), two_sided_primes.end(), 0);
}

int main() {
  std::map<int, bool> is_prime_cache = {{1, false}, {2, true}};
  std::cout << p037() << std::endl;
  return 0;
}

