/*
 * file:    p007.cc
 * title:   10001st prime
 * author:  Alexander P. Gosselin
 * e-mail:  <alexandergosselin@gmail.com>
 * date:    May 23, 2016
 * 
 * link:    <https://projecteuler.net/problem=7>
 */

#include <cmath>
#include <iostream>
#include <vector>
using namespace std;

static const int N = 10001;

// Upper bound on the nth prime.
int prime_upper_bound(int n) {
  return floor(n*(log(n) + log(log(n))));
}

int main() {
  vector<bool> is_prime(prime_upper_bound(N)/2, true);
  size_t prime_count = 1; // Counting 2.
  for (size_t i = 1; i < is_prime.size(); i++) {
    if (!is_prime[i]) continue;
    size_t p = 2*i + 1;
    prime_count++;
    if (prime_count == N) {
      cout << p << endl;
      return 0;
    }
    for (size_t j = i*(p + 1); j < is_prime.size(); j += p) {
      is_prime[j] = false;
    }
  }
}
