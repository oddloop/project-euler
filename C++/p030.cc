/*
 * file:   p030.cc
 * title:  Digit fifth powers
 * author: Alexander Gosselin
 * e-mail: <alexandergosselin@gmail.com>
 * date:   November 10, 2013
 * 
 * link:   <https://projecteuler.net/problem=30>
 */
 
 #include <cmath>
 #include <iostream>
 using namespace std;
 
 int main(int argc, char **argv)
 {
   int fifth_powers [10] = {0,1,pow(2,5),pow(3,5),pow(4,5),pow(5,5),pow(6,5),
                            pow(7,5),pow(8,5),pow(9,5)};
   int sum = 0;
   for(int number = 2; number < 354294; number++) {
     string number_string = to_string(number);
     int number_digit_fifth_power_sum = 0;
     for(auto digit : number_string){
       number_digit_fifth_power_sum += fifth_powers[digit-'0'];
     }
     if(number == number_digit_fifth_power_sum) {
       //cout << number << endl;
       sum += number;
     }
   }
   cout << sum << endl;
 }
