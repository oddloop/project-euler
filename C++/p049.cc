/*
file:   p049.cc
title:  Prime permutations
author: Alexander Gosselin
email:  <alex@oddloop.ca>
date:   December 4, 2018

link:   <https://projecteuler.net/problem=49>
*/

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int isqrt(int n) {
  // integer square root
  if (n == 0 || n == 1) {
    return n;
  }
  int x = n/2;
  for (int y; ; x = y) {
    y = (x + (n/x))/2;
    if (y >= x) {
      break;
    }
  }
  return x;
}


bool is_prime(int n) {
  // trial division, suitable for small n
  if (n % 2 == 0) {
    return n == 2;
  }
  for (int i = 3; i <= isqrt(n); i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

int ipow(int n, int k) {
  // integer exponentiation by squaring
  if (k == 0) {
    return 1;
  }
  int x = 1;
  while (k > 0) {
    if (k & 1) { // e is odd
      x *= n;
    }
    n *= n;
    k /= 2;
  }
  return x;
}


std::string p049(int d = 4, std::string exclude = "1478") {
  // map from digit sets to primes
  std::map<std::string, std::vector<int>> digits_primes;
  for (int n = ipow(10, d - 1) + 1; n < ipow(10, d) - 1; n += 2) {
    if (is_prime(n)) {
      std::string digits_n = std::to_string(n);
      std::sort(digits_n.begin(), digits_n.end());
      if (digits_n == exclude) {
	continue;
      }
      for (int p : digits_primes[digits_n]) {
	int q = 2*n - p;
	std::string digits_q = std::to_string(q);
	std::sort(digits_q.begin(), digits_q.end());
	if ((digits_q == digits_n) && is_prime(q)) {
	  return std::to_string(p) + std::to_string(n) + std::to_string(q);
	}
      }
      digits_primes[digits_n].push_back(n);
    }
  }
  return "";
}

int main() {
  std::cout << p049() << std::endl;
  return 0;
}
