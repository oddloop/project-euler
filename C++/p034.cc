/*
 * file:   p034.cc
 * title:  Digit factorials
 * author: Alexander Gosselin
 * e-mail: <alexandergosselin@gmail.com>
 * date:   December 16, 2013
 * 
 * link:   <https://projecteuler.net/problem=34>
 */

#include <chrono>
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
  auto start = chrono::system_clock::now();
  
  int factorial[10] = {1,1,2,6,24,120,720,5040,40320,362880};
  int digit_factorial_sum = 0;
  //two digits
  for(size_t a = 1; a < 10; a++) {
    for(size_t b = 0; b < 10; b++) {
      int number = 10*a+b;
      if(factorial[a]+factorial[b] == number) {
        digit_factorial_sum += number;
      }
    }
  }
  //three digits
  for(size_t a = 1; a < 10; a++) {
    for(size_t b = 0; b < 10; b++) {
      for(size_t c = 0; c < 10; c++) {
        int number = 100*a+10*b+c;
        if(factorial[a]+factorial[b]+factorial[c] == number) {
          digit_factorial_sum += number;
        }
      }
    }
  }
  //four digits
  for(size_t a = 1; a < 10; a++) {
    for(size_t b = 0; b < 10; b++) {
      for(size_t c = 0; c < 10; c++) {
        for(size_t d = 0; d < 10; d++) {
          int number = 1000*a+100*b+10*c+d;
          if(factorial[a]+factorial[b]+factorial[c]+factorial[d] == number) {
            digit_factorial_sum += number;
          }
        }
      }
    } 
  }
  //five digits
  for(size_t a = 1; a < 10; a++) {
    for(size_t b = 0; b < 10; b++) {
      for(size_t c = 0; c < 10; c++) {
        for(size_t d = 0; d < 10; d++) {
          for(size_t e = 0; e < 10; e++) {
            int number = 10000*a+1000*b+100*c+10*d+e;
            if(factorial[a]+factorial[b]+factorial[c]+factorial[d]+
               factorial[e] == number) {
              digit_factorial_sum += number;
            }
          }
        }
      }
    } 
  }
  cout << digit_factorial_sum << endl << endl;
  auto end = chrono::system_clock::now();
  auto elapsed = chrono::duration_cast<std::chrono::milliseconds>(end - start);
  cout << "Time elapsed: " <<  elapsed.count() << "ms" << endl;
}
