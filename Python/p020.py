#!/usr/bin/env python3
"""
file:   p020.py
title:  Factorial digit sum
author: Alexander P. Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   November 29, 2018

link:   <https://projecteuler.net/problem=20>
"""

from functools import reduce
from operator import mul


def factorial(n: int) -> int:
    """Return n!"""
    return reduce(mul, range(1, n + 1))


def digit_sum(n: int) -> int:
    """Return the digit sum of n"""
    return sum(int(d) for d in str(n))


def factorial_digit_sum(n: int) -> int:
    """Return the sum of the digits in n!"""
    return digit_sum(factorial(n))


def p020(n: int = 100) -> int:
    """Return the solution to Project Euler Problem 20"""
    return factorial_digit_sum(n)


def main() -> None:
    print(p020())


if __name__ == "__main__":
    main()
