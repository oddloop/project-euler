#!/usr/bin/env python3
"""
file:   p053.py
title:  Combinatoric selections
author: Alexander Gosselin
email:  <alex@oddloop.ca>
date:   December 9, 2018

link:   <https://projecteuler.net/problem=53>
"""

def count_nCr_greater_than(max_n, lim_nCr):
    count = 0
    C = [1, 1] + [0]*(max_n - 1)
    for n in range(2, max_n + 1):
        C[n - 1] = 1
        #r = n - 1
        for r in range(n - 1, 0, -1):
            C[r] += C[r - 1]
            if C[r] > lim_nCr:
                count += 2*r - n + 1
                break
        # while r > 0:
        #     C[r] += C[r - 1]
        #     if C[r] > lim_nCr:
        #         count += 2*r - n + 1
        #         C[r] = k
        #         r = 0
        #     else:
        #         r -= 1
    return count

def p053(max_n=100, lim_nCr=1000000):
    return count_nCr_greater_than(max_n, lim_nCr)

def main():
    print(p053())

if __name__ == "__main__":
    main()
    
