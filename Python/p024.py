#!/usr/bin/env python3
"""
file:   p024.py
title:  Lexicographic permutations
author: Alexander Gosselin
e-mail: <alex@oddloop.ca>

link:   <https://projecteuler.net/problem=24>
"""

#from functools import reduce
from itertools import accumulate, chain
from operator import mul
from typing import Sequence

# def factorial(n:int) -> int:
#     """Return the nth factorial"""
#     return reduce(mul, range(1, n + 1), 1)

def factorials(n:int) -> accumulate:
    """Return the first n factorials: 0! through (n-1)!"""
    return accumulate(chain((1,), range(1, n)), mul)

def nth_permutation(s:Sequence, n:int) -> list:
    """Return the nth permutation of s formatted as a list"""
    remain = list(s)
    result = []
    # n %= factorial(len(s)) # ensure n < len(s)!
    for d in reversed(tuple(factorials(len(s)))):
        q, n = divmod(n, d)
        result.append(remain.pop(q))
    return result
        
def p024(s:str="0123456789", n:int=1000000) -> str:
    """Return the solution to Project Euler Problem 24"""
    return "".join(nth_permutation(s, n - 1))
    
def main() -> None:
    print(p024())

if __name__ == "__main__":
    main()
