#!/usr/bin/env python3
"""
file:   p001.scm
title:  Multiples of 3 and 5
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
        <alexander.gosselin@alumni.ubc.ca>
date:   November 21, 2018

link:   <https://projecteuler.net/problem=1>
"""


def gcd(a: int, b: int) -> int:
    """Return the greatest common divisor of a and b."""
    while b:
        a, b = b, a % b
    return a


def lcm(a: int, b: int) -> int:
    """Return the least common multiple of a and b."""
    return (a * b) // gcd(a, b)


def triangle(n: int) -> int:
    """Return the nᵗʰ triangle number."""
    return (n * (n + 1)) // 2


def smlt(x: int, n: int) -> int:
    """Return the sum of multiples of x that are less than n."""
    return x * triangle((n - 1) // x)


def smlt2(a: int, b: int, n: int) -> int:
    """Return the sum of multiples of a or b that are less than n."""
    return smlt(a, n) + smlt(b, n) - smlt(lcm(a, b), n)


def p001() -> int:
    """Return the solution to Project Euler Problem 1."""
    return smlt2(3, 5, 1000)


def main() -> None:
    print(p001())


if __name__ == "__main__":
    main()
