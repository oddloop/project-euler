#!/usr/bin/env python3
'''
file:   p007.py
title:  10001st Prime
author: Alexander P. Gosselin
e-mail: <alexandergosselin@gmail.com>
date:   May 28, 2016

link:   <https://projecteuler.net/problem=7>
'''

from math import log

def prime_upper_bound(n):
    return n*log(n*log(n))

# Sieve of Eratosthenes
def prime(n):
    c = 1 # prime count
    m = int(prime_upper_bound(n))//2
    is_prime = [True]*m
    for i in range(1, m):
        if is_prime[i]:
            p = 2*i + 1
            c += 1
            if c == n:
                return p
            for j in range(i*(p + 1), m, p):
                is_prime[j] = False

def main():
  n = 10001
  print(prime(n))

if __name__ == "__main__":
    main()
