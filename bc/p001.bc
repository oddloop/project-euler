#!/usr/bin/bc -q
/*
file:   p001.bc
title:  Multiples of 3 and 5
author: Alexander Gosselin
e-mail: <alexandergosselin@gmail.com>
        <alexander.gosselin@alumni.ubc.ca>
date:   April 9, 2017

link:   <https://projecteuler.net/problem=1>

comment:  
  bc is good for small problems!
*/

/* the nth triangle number */
define triangle(n) {
  return n*(n + 1)/2;
}

/* greatest common divisor
 <https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations> */
define gcd(a, b) {
  while (b) {
    t = b;
    b = a % b;
    a = t;
  }
  return a;
}
  
/* least common multiple */
define lcm(a, b) {
  return a*b/gcd(a,b);
}

/* sum of multiples of x less than n */
define smlt(x, n) {
  return x*triangle((n - 1)/x); 
}

/* sum of multiples of x or y less than n */
define smlt2(x, y, n) {
  return smlt(x, n) + smlt(y, n) - smlt(lcm(x, y), n);
}

/* return the solution */
define p001() {
  return smlt2(3, 5, 1000);
}

print p001(), "\n";
quit
