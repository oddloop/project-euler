#|
file:   p006.lisp
title:  Sum square difference
author: Alexander P. Gosselin
mail:   <alexandergosselin@gmail.com>
date:   February 14, 2016

link:   <https://projecteuler.net/problem=6>
|#

(defun triangular-number (n)
  (/ (* n (+ n 1)) 2))

(defun square-pyramidal-number (n)
  (/ (* n (+ n 1) (+ (* 2 n) 1)) 6))

(defun sum-square-difference (n)
  (- (expt (triangular-number n) 2) (square-pyramidal-number n)))

(defun p006 () (sum-square-difference 100))

(prin1 (p006))
(write-char #\linefeed)
