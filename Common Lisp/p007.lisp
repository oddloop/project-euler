;;; file:   p007.lisp
;;; title:  10001st prime
;;; author: Alexander Gosselin
;;; e-mail: <alexandergosselin@gmail.com>
;;;         <alexander.gosselin@alumni.ubc.ca>
;;; date:   February 14, 2016
;;; 
;;; link:   <https://projecteuler.net/problem=7>

;; <http://stackoverflow.com/a/13937652>
(defun range (max &key (min 0) (step 1))
  "Returns a list of numbers from min to max, incrementing by
   step. min and step are optional arguments, and default to 0
   and 1 respectively."
  (loop for n from min below max by step
     collect n))

(defun sieve-of-eratosthenes (n)
  "Returns a list of prime numbers from 2 to n."
  (let ((primes (range n :min 3 :step 2)))
    (loop for p in primes do
	 (let ((c (* p p)))
	   (loop while (< c n) do
		(setq primes (delete c primes))
		(setq c (+ c p p)))))
    (return-from sieve-of-eratosthenes
      (cons 2 primes))))

;; <https://en.wikipedia.org/wiki/Prime-counting_function>
(defun prime-upper-bound (n)
  "Returns the smallest number k guaranteed to be larger than the
   nth prime number."
  (* n (+ (log n) (log (log n)))))

(defun nth-prime (n)
  (nth (- n 1) (sieve-of-eratosthenes
		(prime-upper-bound n))))

(defun p007 () (nth-prime 10001))

(prin1 (p007))
(write-char #\linefeed)
